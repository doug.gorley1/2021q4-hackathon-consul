# Hashicorp Consul for Service Configuration

These rough notes capture a bunch of the commands used to deploy
my project using kind, kubectl, docker, and helm.

## Installing kind
```
$ brew install kind
$ kind create cluster
$ kubectl cluster-info --context kind-kind

Kubernetes control plane is running at https://127.0.0.1:64871
CoreDNS is running at https://127.0.0.1:64871/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

## Observing the kind cluster
```
$ kubectl get pods -A
NAMESPACE            NAME                                         READY   STATUS    RESTARTS   AGE
kube-system          coredns-558bd4d5db-rmfjv                     1/1     Running   0          3m1s
kube-system          coredns-558bd4d5db-tmjdv                     1/1     Running   0          3m1s
kube-system          etcd-kind-control-plane                      1/1     Running   0          3m9s
kube-system          kindnet-nwtp4                                1/1     Running   0          3m1s
kube-system          kube-apiserver-kind-control-plane            1/1     Running   0          3m9s
kube-system          kube-controller-manager-kind-control-plane   1/1     Running   0          3m15s
kube-system          kube-proxy-pqsbw                             1/1     Running   0          3m1s
kube-system          kube-scheduler-kind-control-plane            1/1     Running   0          3m15s
local-path-storage   local-path-provisioner-547f784dff-7tj9p      1/1     Running   0          3m1s
```

## Installing Consul in kind
```
$ helm repo add hashicorp https://helm.releases.hashicorp.com

$ helm search repo hashicorp/consul
NAME            	CHART VERSION	APP VERSION	DESCRIPTION
hashicorp/consul	0.28.0       	1.9.1      	Official HashiCorp Consul Chart

$ helm install consul hashicorp/consul --set global.name=consul
$ kubectl port-forward service/consul-server 8500:8500

# Consul wouldn't run properly out of the box because of it's default
# anti-affinity settings, and the fact that all 3 Consul nodes were
# running on the single kind node.  Disabling the affinity settings
# solved this problem.

$ cat values.yaml
server:
  affinity: {}
$ helm uninstall consul
$ helm install -f ./values.yaml consul hashicorp/consul --set global.name=consul
```

## Pushing images to the kind control plane
```
#!/bin/bash
#
# Build and push the service.

IMAGE_NAME="hack-bravo"
SEMVER=$1

docker build --tag $IMAGE_NAME:$SEMVER .
kind load docker-image $IMAGE_NAME:$SEMVER
```

## Port forwarding kind services to the local machine
```
kubectl port-forward service/hack-alpha-test 8001:http
kubectl port-forward service/hack-bravo 8001:http
```

## Running consul-template
```
./consul-template -config config.hcl
```
