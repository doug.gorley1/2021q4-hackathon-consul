package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"text/template"
	"time"

	"github.com/spf13/viper"
)

type DisplaySettings struct {
	Background string
	Message    string
	PageTitle  string
}

var displaySettings DisplaySettings

func rootHandler(w http.ResponseWriter, req *http.Request) {
	tmpl, err := template.ParseFiles("index.html")
	if err != nil {
		log.Println("Couldn't parse template file")
	}
	tmpl.Execute(w, displaySettings)
}

func configHandler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "configHandler\n")
}

func livenessHandler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "live!\n")
}

func readinessHandler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "ready!\n")
}

func readConfig() {
	// Read the local config from file
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath("/etc")
	err := viper.ReadInConfig()
	if err != nil {
		log.Println(err)
	}

	// Unmarshal the display settings
	err = viper.Sub("display").Unmarshal(&displaySettings)
	if err != nil {
		log.Printf("Unable to decode into struct, %v\n", err)
	}
}

func writePid() {
	pid := os.Getpid()
	pidBytes := []byte(strconv.Itoa(pid))
	log.Printf("PID: %d", pid)
	err := os.WriteFile("hack-bravo.pid", pidBytes, 0644)
	if err != nil {
		log.Println("Error occurred writing pid file.")
	}
}

func main() {
	writePid()
	readConfig()

	// Handle SIGHUP by reloading config
	sigChannel := make(chan os.Signal, 2)
	signal.Notify(sigChannel, syscall.SIGHUP)

	go func() {
		for {
			<-sigChannel
			readConfig()
			log.Println("Config change detected via SIGHUP, reloaded")
		}
	}()

	// Check for /etc/reload_config file, reload if exists
	go func() {
		for {
			if _, err := os.Stat("/etc/reload_config"); err == nil {
				os.Remove("/etc/reload_config")
				if err != nil {
					log.Println(err)
				}
				readConfig()
				log.Println("Config change detected via flag file, reloaded")
			}
			time.Sleep(5 * time.Second)
		}
	}()

	// Set the route handlers
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/config", configHandler)
	http.HandleFunc("/liveness", livenessHandler)
	http.HandleFunc("/readiness", readinessHandler)

	// Listen and serve
	log.Println("Listing for requests")
	log.Fatal(http.ListenAndServe(":80", nil))
}
