consul {
    address = "consul-server.default.svc.cluster.local:8500"
    retry {
        enabled = true
        attempts = 12
        backoff = "250ms"
    }
}
reload_signal = "SIGHUP"
kill_signal = "SIGINT"
max_stale = "10m"
log_level = "warn"

wait {
    min = "5s"
    max = "10s"
}

template {
    source      = "./config.json.tmpl"
    destination = "./config.json"
    command     = "kill -s HUP $(cat hack-bravo.pid)"
}

vault {
    renew_token = false
}