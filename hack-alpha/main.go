package main

import (
	"io"
	"log"
	"net/http"
	"text/template"
	"time"

	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

type DisplaySettings struct {
	Background string
	Message    string
	PageTitle  string
}

var displaySettings DisplaySettings

func rootHandler(w http.ResponseWriter, req *http.Request) {
	tmpl, err := template.ParseFiles("index.html")
	if err != nil {
		log.Println("Couldn't parse template file")
		log.Println(err)
	}
	err = tmpl.Execute(w, displaySettings)
	if err != nil {
		log.Println("Couldn't execute template")
		log.Println(err)
	}
}

func configHandler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "configHandler\n")
}

func livenessHandler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "live!\n")
}

func readinessHandler(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "ready!\n")
}

func main() {
	// Read the remote config from Consul (consul-server.default.svc.cluster.local)
	// viper.AddRemoteProvider("consul", "localhost:8500", "config/hack-alpha")
	viper.AddRemoteProvider("consul", "consul-server.default.svc.cluster.local:8500", "config/hack-alpha")
	viper.SetConfigType("yaml")
	err := viper.ReadRemoteConfig()
	if err != nil {
		log.Println(err)
	}

	// Unmarshal the display settings
	err = viper.Sub("display").Unmarshal(&displaySettings)
	if err != nil {
		log.Printf("Unable to decode into struct, %v\n", err)
	}
	log.Println(displaySettings.Background)

	// open a goroutine to watch remote changes forever
	go func() {
		for {
			time.Sleep(time.Second * 5) // delay after each request

			err := viper.WatchRemoteConfig()
			if err != nil {
				log.Printf("unable to read remote config: %v", err)
				continue
			}

			// unmarshal new config into our runtime config struct. you can also use channel
			// to implement a signal to notify the system of the changes
			err = viper.Sub("display").Unmarshal(&displaySettings)
			if err != nil {
				log.Printf("Unable to decode into struct, %v\n", err)
			}
		}
	}()

	// Set the route handlers
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/config", configHandler)
	http.HandleFunc("/liveness", livenessHandler)
	http.HandleFunc("/readiness", readinessHandler)

	// Listen and serve
	log.Println("Listing for requests")
	log.Fatal(http.ListenAndServe(":80", nil))
}
